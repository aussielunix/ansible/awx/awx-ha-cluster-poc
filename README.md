# awx-ha-cluster

## Introduction

**Docs are a Work In Progress**  

[AWX](https://github.com/ansible/awx) is originally designed to run in a Kubernetes environment only.  
This code allows you to install and use AWX on VMs (or bare metal) - without Kubernetes !

Before installation you should have an understanding of AWX in general, AWX node types (control, hybrid, hop, execution) and Ansible.

![receptor mesh architecture](docs/awx_mesh_arch1.png)

Some great blog posts to get you up to speed are:

* https://www.ansible.com/blog/peeling-back-the-layers-and-understanding-automation-mesh
* https://www.ansible.com/blog/the-anatomy-of-automation-execution-environments
* https://www.ansible.com/blog/when-localhost-isnt-what-it-seems-in-red-hat-ansible-automation-platform-2

## Demo Architecture

4 node AWX Cluster

* 1 x Caddy reverse proxy with automatic LetsEncrypt certificate (optional)
* 1 x postgres server node (optional)
* 1 x AWX Control node
* 2 x AWX Worker nodes

**Control node is a collection of containers that run AWX, Redis and Postgres**  
**At this stage this repo is not capable of building hop nodes**

## Dependencies

* Ansible 2.9+
* 2 or more RedHat Linux (or RHEL Family)  instances - VM or bare metal
* non root user with sudo access on each Linux host
* working hostname resolution (with fqdn) between all hosts (DNS records)
* :FIXME: firewall ports open between all nodes

## Building the demo

This assumes all nodes are provisioned and you have ssh access as a non-root
user with sudo access.  
We use Ansible to build and operate the cluster.  

## Prepare Ansible

* Tune the [inventory file](demo/inventory/valueline/inventory)
* Tune some AWX [variables](demo/inventory/valueline/group_vars/all/awx.yml)
* Check ansible connectivity

  ```bash
  cd demo
  ansible -i inventory/valueline -m ping
  ```

## Ansible Playbook flags

```txt
**Note** If you run the main playbook without any extra args it will do nothing.  
Possible extra args are:
  * -e prepare_hosts=true               # fixes up hostname and fqdn of host
  * -e proxy=true                       # Configure node with Caddy as a reverse proxy
  * -e db=true                          # configure a dedicated postgres node
  * -e controllers=true                 # Configure AWX Controller nodes
  * -e workers=true                     # Configure AWX Worker nodes
  * -e tlsgen=true                      # generate new TLS certs for receptor (workers)
  * -e awx_node_action=register         # register new worker node with controller
  * -e awx_node_action=unregister       # unregister worker node from controller
```

## Provision AWX cluster with postgres as a local container and no external reverse proxy

This requires just 2 nodes.  
One controller and one worker. All services on the
controller are containerised - redis, postgres and several AWX specific
containers.

```bash
ansible-playbook -i inventory/internaldb main.yml \
  -e prepare_hosts=true

ansible-playbook -i inventory/internaldb main.yml \
  -e controllers=true \
  -e workers=true \
  -e awx_node_action=register \
  -e tlsgen=true
```

## Provision Cluster with external postgres node and external reverse proxy

This requires 4 or more nodes.  
It will setup postgres on one as a dedicated db node, Caddy on another as a reverse proxy, a controller and one or more worker nodes. All services on the controller are containerised.

```bash
ansible-playbook -i inventory/valueline main.yml \
  -e prepare_hosts=true \
  -e db=true \
  -e proxy=true

ansible-playbook -i inventory/valueline main.yml \
  -e controllers=true \
  -e workers=true \
  -e awx_node_action=register \
  -e tlsgen=true
```

* ssh to the control node and optionally, add demo data

  ```bash
  sudo docker exec -ti awx-task bash
  awx-manage create_preload_data
  exit
  ```

Open `http://awxpoc-control-1.hl.valueline.io:8052` in your browser or point
your reverse proxy / load balancer at this address.

## AWX Operations

### Add extra worker node to cluster

```bash
# Add new node to the `[workers]` group in the inventory

ansible-playbook -i inventory/internaldb main.yml \
  -e workers=true \
  -e awx_node_action=register \
  -e tlsgen=true
```

### Remove worker node from cluster

```bash
ansible-playbook -i inventory/internaldb main.yml \
  -e workers=true \
  -e awx_node_action=unregister \
  --limit awxpoc-worker-3.hl.valueline.io

# remove node from the `[workers]` group in the inventory
```

### Upgrade AWX

:TODO:

### Patch Linux

:TODO:

### Backup AWX

:TODO:

### Restore AWX

:TODO:

## Thoughts

* In a future version, Redis and Postgres could be run seperately, either via containers  
or other external service provider, eg: RDS or ElastiCache
* In a future version, the RedHat Ansible Automation Platform [source rpms](https://ftp.redhat.com/pub/redhat/linux/enterprise/9Base/en/AnsibleAutomationPlatform/SRPMS/) are used to build rpms that allow the control node to be run without containers.

