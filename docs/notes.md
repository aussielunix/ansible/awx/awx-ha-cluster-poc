# Raw Notes

These should be turned into docs at some stage.

### Provision Cluster with external postgres and external reverse proxy

```
ansible-playbook -i inventory/valueline main.yml -e prepare_hosts=true -e db=true -e proxy=true
ansible-playbook -i inventory/valueline main.yml -e controllers=true -e workers=true -e awx_node_action=register -e tlsgen=true
```

### Provision cluster with postgres as a local container and no external reverse proxy

```
ansible-playbook -i inventory/internaldb main.yml -e prepare_hosts=true
ansible-playbook -i inventory/internaldb main.yml -e controllers=true -e workers=true -e awx_node_action=register -e tlsgen=true
```

### Add extra worker node to cluster

```
# Add new node to the `[workers]` group in the inventory
ansible-playbook -i inventory/internaldb main.yml -e workers=true -e awx_node_action=register -e tlsgen=true
```

### Remove worker node from cluster

```
ansible-playbook -i inventory/internaldb main.yml -e workers=true -e awx_node_action=unregister --limit awxpoc-worker-3.hl.valueline.io
# remove node from the `[workers]` group in the inventory
```

All possible flags for the `main` playbook:

```
-e prepare_hosts=true
-e db=true
-e proxy=true
-e controllers=true
-e workers=true
-e tlsgen=true
-e awx_node_action=register
-e awx_node_action=unregister
```

### Provision a Hybrid Node

Set `awx_node_role_type=hybrid`  
Set `awx_ee_image_url: docker.io/t42x/awx-ha-cluster-ee:latest`  
Because we are running AWX with `docker-compose`, we need a modified `ee` container so that jobs will run with Podman inside of the EE docker container.

